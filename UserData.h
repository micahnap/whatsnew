//
//  UserData.h
//  WhatsNew
//
//  Created by Micah on 11/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Articles.h"

@interface UserData : NSObject

@property (nonatomic, assign) NSInteger gainedScore;
@property (nonatomic, assign) NSInteger totalScore;
@property (nonatomic, assign) NSInteger questionCount;
@property (nonatomic, assign) bool userGuessIt;
@property (nonatomic, copy) void (^customAppearanceBlock)(id);

+(UserData *) dataObj;
-(void)saveUserDataToDisk;
-(void)loadDataFromDisk;
@end
