//
//  RightTimeViewController.h
//  WhatsNew
//
//  Created by Micah on 8/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Articles.h"

@interface RightTimeViewController : UIViewController
@property (nonatomic, strong) Articles *article;
@property (nonatomic, copy) void (^dismissAndLoadNewQuestionBlock)(void);
@end
