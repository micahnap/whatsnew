//
//  UserData.m
//  WhatsNew
//
//  Created by Micah on 11/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import "UserData.h"

@implementation UserData

+(UserData *) dataObj;
{
    static UserData * single=nil;
    
    @synchronized(self)
    {
        if(!single)
        {
            single = [UserData new];
            
        }
    }
    return single;
}

-(void)saveUserDataToDisk{
    UserData *userData = [UserData dataObj];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger:userData.questionCount forKey:@"QuestionCount"];
    [userDefaults setInteger:userData.totalScore forKey:@"TotalScore"];
    [userDefaults synchronize];
}

-(void)loadDataFromDisk{
    UserData *userData = [UserData dataObj];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    userData.questionCount = [userDefaults integerForKey:@"QuestionCount"];
    userData.totalScore = [userDefaults integerForKey:@"TotalScore"];
}
@end
