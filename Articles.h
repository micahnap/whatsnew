//
//  Articles.h
//  WhatsNew
//
//  Created by Micah on 7/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Articles : NSObject

@property (nonatomic, strong) NSNumber *correctAnswerIndex;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *standFirst;
@property (nonatomic, strong) NSString *storyUrl;
@property (nonatomic, strong) NSString *section;
@property (nonatomic, strong) NSArray *headlines;
@property (nonatomic, strong) UIImage *imageArticle;


-(id)initWithImageUrl:(NSString *)imageUrl standFirst:(NSString *)standFirst storyURL:(NSString *)storyURL section:(NSString *)section headlines:(NSArray *)headlines andCorrectAnswerIndex:(NSNumber *)correctAnswerIndex;

+(void)fetchArticlesWithCompletion:(void (^)(NSArray *, NSError *))completion;
@end
