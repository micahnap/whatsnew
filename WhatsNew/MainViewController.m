//
//  ViewController.m
//  WhatsNew
//
//  Created by Micah on 7/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import "MainViewController.h"
#import "RightTimeViewController.h"
#import "Articles.h"
#import "UserData.h"

#define NEWSURL @"https://dl.dropboxusercontent.com/u/30107414/game.json"
#define ARTICLE_VALUE 12

@interface MainViewController ()
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *newsArticles;
@property (nonatomic, strong) Articles *article;
@property (nonatomic, strong) UserData *userData;
@property (nonatomic, strong) NSUserDefaults *userDefaults;
@property (nonatomic, assign) NSInteger ticker;
@property (weak, nonatomic) IBOutlet UILabel *lblHeadline;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBG;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewArticle;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *buttonCollection;

@end

@implementation MainViewController
bool guessedOnce;

void(^customAppearanceBlock)(id sender);

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userData = [UserData dataObj];
    [self.userData loadDataFromDisk];
    self.ticker = 10;
    
    [self customiseUI];
    [self loadArrayWithArticles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)customiseUI{
    //storing appearnace code for later use
    __weak typeof(self)weakSelf = self;
    customAppearanceBlock = ^(id sender){
        __strong typeof(weakSelf)strongSelf = sender;
        for (UIView *view in strongSelf.view.subviews){
            if (view.tag != 999) {
                view.layer.borderWidth= 1.0f;
                view.layer.borderColor = [[UIColor whiteColor] CGColor];
            }
        }
        strongSelf.imageViewBG.alpha = 0.3;
    };
    
    customAppearanceBlock(weakSelf);
    self.userData.customAppearanceBlock = customAppearanceBlock;
}

#pragma mark - Utilities
-(void)loadArrayWithArticles{
    
    [self hideViews:true];
    
    [Articles fetchArticlesWithCompletion:^(NSArray *array, NSError *error){
        if (!error) {
                self.newsArticles = array;
                [self updateUI];
                [self hideViews:false];
        }else{
            [self showAlertForTitle:@"Error" andMessage:error.localizedDescription withCompletion:^{
                [self loadArrayWithArticles];
            }];
        }
    }];
}

-(void)updateUI{
    //populate image views and labels with dictionary data
    if (self.userData.questionCount + 1 > [self.newsArticles count]) {
        self.userData.questionCount = 0;
        [self loadArrayWithArticles];
    }else{
        self.article = self.newsArticles[self.userData.questionCount];
        
        dispatch_async(dispatch_queue_create("imageLoader", NULL), ^{
            self.article.imageArticle = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.article.imageURL]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageViewBG.image = self.article.imageArticle;
                self.imageViewArticle.image = self.article.imageArticle;
                [self.buttonCollection[0] setTitle:self.article.headlines[0] forState:UIControlStateNormal];
                [self.buttonCollection[1] setTitle:self.article.headlines[1] forState:UIControlStateNormal];
                [self.buttonCollection[2] setTitle:self.article.headlines[2] forState:UIControlStateNormal];
                self.userData.gainedScore = ARTICLE_VALUE;
                [self startTimer];
            });
        });
    }
}

- (void)startTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownProgress) userInfo:nil repeats:NO];
}

- (void)stopTimer{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.timer invalidate];
    });
}

- (void)showAlertForTitle:(NSString *)title andMessage:(NSString *)message withCompletion:(void(^)(void))completionBlock{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                       completionBlock();
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)hideViews:(BOOL)hidden{
    for (UIView *view in self.view.subviews){
        view.hidden = hidden;
    }
    hidden ? [self.activityIndicator startAnimating] : [self.activityIndicator stopAnimating];
}

-(void)launchScoreScreenFromSender:(UIButton *)sender{
    
    self.ticker = 10;
    self.userData.questionCount ++;
    self.userData.totalScore += self.userData.gainedScore;
    [self stopTimer];
    [self.userData saveUserDataToDisk];
    
    guessedOnce = false;
    self.userData.userGuessIt = sender ? true : false;
    [self performSegueWithIdentifier:@"scoreScreen" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UINavigationController *nav = segue.destinationViewController;
    RightTimeViewController *rtVC = (RightTimeViewController *)nav.viewControllers[0];
    rtVC.article = self.article;
    rtVC.dismissAndLoadNewQuestionBlock = ^{
        [self dismissViewControllerAnimated:YES completion:nil];
        [self updateUI];
    };
}

-(void)countDownProgress{
    self.ticker --;
    if (self.userData.gainedScore > 0){
        self.userData.gainedScore --;
    }
    
    self.lblHeadline.text = [NSString stringWithFormat:@"Guess the headline for %li points!", (long)self.userData.gainedScore];
    [self.progressView setProgress:self.ticker * 0.1];
    
    if (self.ticker == 0) {
        self.userData.gainedScore = 0;
        [self launchScoreScreenFromSender:nil];
    }else{
        [self startTimer];
    }
}

#pragma mark - buttons
- (IBAction)buttonsPressed:(UIButton *)sender{
    
    if ([self.article.correctAnswerIndex isEqualToNumber:[NSNumber numberWithInt:(unsigned)sender.tag]]) {
        [self launchScoreScreenFromSender:sender];
    }else{
        [self stopTimer];
        if (!guessedOnce) {
            guessedOnce = true;
            self.userData.gainedScore -= 2;
        }
        [self showAlertForTitle:@"Nope!" andMessage:@"Guess again!" withCompletion:^{
           [self startTimer];
        }];
    }
}
@end
