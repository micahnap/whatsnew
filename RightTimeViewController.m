//
//  RightTimeViewController.m
//  WhatsNew
//
//  Created by Micah on 8/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import "RightTimeViewController.h"
#import "WebViewController.h"
#import "UserData.h"

@interface RightTimeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UILabel *lblgainedScore;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalScore;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewArticle;
@property (weak, nonatomic) IBOutlet UITextView *txtViewStandFirst;
@property (weak, nonatomic) IBOutlet UITextView *txtViewHeading;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBG;
@property (weak, nonatomic) IBOutlet UIButton *btnReadOn;
@property (strong, nonatomic) UserData *userData;
@end

@implementation RightTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userData = [UserData dataObj];
    
    __weak typeof(self)weakSelf = self;
    self.userData.customAppearanceBlock(weakSelf);
    [self loadUI];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIView animateWithDuration:1.0 animations:^{
        [self.lblgainedScore setAlpha:1.0];
    }completion:^(BOOL complete){
        [UIView animateWithDuration:0.5 animations:^{
            [self.lblTotalScore setAlpha:1.0];
        }completion:^(BOOL complete){
            [UIView animateWithDuration:0.5 animations:^{
                [self.txtViewStandFirst setAlpha:1.0];
                [self.btnReadOn setAlpha:1.0];
            }completion:nil];
        }];
    }];
}

-(void)loadUI{
    self.lblTotalScore.text = [NSString stringWithFormat:@"Your total score is %li points", (long)self.userData.totalScore];
    self.lblgainedScore.text = [NSString stringWithFormat:@"You earned %li points!", (long)self.userData.gainedScore];
    self.lblHeading.text = self.userData.userGuessIt ? @"CORRECT" : @"TIME'S UP!";
    self.imageViewArticle.image = self.article.imageArticle;
    self.txtViewStandFirst.text = self.article.standFirst;
    self.txtViewHeading.text = self.article.headlines[[self.article.correctAnswerIndex integerValue]];
    self.imageViewBG.image = self.article.imageArticle;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)nextArticlePressed:(id)sender {
    self.dismissAndLoadNewQuestionBlock();
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    WebViewController *webVC = (WebViewController *)segue.destinationViewController;
    webVC.storyURL = self.article.storyUrl;
}

@end
