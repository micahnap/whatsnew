//
//  Articles.m
//  WhatsNew
//
//  Created by Micah on 7/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import "Articles.h"
#define NEWSURL @"https://dl.dropboxusercontent.com/u/30107414/game.json"

@implementation Articles

-(id)initWithImageUrl:(NSString *)imageUrl standFirst:(NSString *)standFirst storyURL:(NSString *)storyURL section:(NSString *)section headlines:(NSArray *)headlines andCorrectAnswerIndex:(NSNumber *)correctAnswerIndex{
    
    self.imageURL = imageUrl;
    self.standFirst = standFirst;
    self.storyUrl = storyURL;
    self.section = section;
    self.headlines = headlines;
    self.correctAnswerIndex = correctAnswerIndex;
    
    return self;
}

+(void)fetchArticlesWithCompletion:(void (^)(NSArray *, NSError *))completion{
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:NEWSURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if(httpResponse.statusCode == 200){
            NSMutableArray *newsArticlesM = [NSMutableArray new];
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSArray *items = responseDict[@"items"];
            
            [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                NSDictionary *itemDict = (NSDictionary *)obj;
                Articles *article = [[Articles alloc] initWithImageUrl:itemDict[@"imageUrl"] standFirst:itemDict[@"standFirst"] storyURL:itemDict[@"storyUrl"] section:itemDict[@"section"] headlines:itemDict[@"headlines"] andCorrectAnswerIndex:itemDict[@"correctAnswerIndex"]];
                [newsArticlesM addObject:article];
            }];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion([newsArticlesM copy],nil);
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil,error);
            });
        }
    }] resume];
}

@end
