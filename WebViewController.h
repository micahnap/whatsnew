//
//  WebViewController.h
//  WhatsNew
//
//  Created by Micah Napier on 9/02/2015.
//  Copyright (c) 2015 Micah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSString *storyURL;
@end
